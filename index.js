const express = require('express');
const socketIO = require('socket.io');
const path = require('path');

const PORT = process.env.PORT || 3000;
const INDEX = path.join(__dirname, '/public/index.html');
const PUBLIC = path.join(__dirname + '/public');

const server = express()
  .use("/public", express.static(PUBLIC))
  .use((req, res) => res.sendFile(INDEX))
  .listen(PORT, () => console.log('Listening on ${ PORT }'));

const io = socketIO(server);

// This is the map and the map size
var gameMap = [[], []];
var width = 29;
var height = 19;

// init with all zero
for (let i = 0; i < height; i++) {
    gameMap[i] = [];
    for (let j = 0; j < width; j++) {
        gameMap[i][j] = 0;
    }
}

// for every user, give him a userId. Start from 2 will have less bug
var userId = 2;

function recalGameMap() {
    var tempGameMap = [[], []];
    for (let i = 0; i < height; i++) {
        tempGameMap[i] = [];
        for (let j = 0; j < width; j++) {
            let count = 0;
            let dict = {};
            
            let upper = i - 1;
            let lower = i + 1;
            let left = j - 1;
            let right = j + 1;
            
            /*
            Check 8 square near the middle
            If it is valid and it is not 0, +1 to dictionary for that userId. Of coz, it will be 1 if it is undefined in the dictionary.
            */
            
            // If upper is valid
            if (upper >= 0) {
                if (left >= 0 && gameMap[upper][left] > 0) { // If left is valid && its value > 0
                    count++;
                    dict[gameMap[upper][left]] = 
                        dict[gameMap[upper][left]] == undefined ? 1 : dict[gameMap[upper][left]] + 1;
                }
                if (gameMap[upper][j] > 0) {
                    count++;
                    dict[gameMap[upper][j]] =
                        dict[gameMap[upper][j]] == undefined ? 1 : dict[gameMap[upper][j]] + 1;
                }
                if (right < width && gameMap[upper][right] > 0) {
                    count++;
                    dict[gameMap[upper][right]] =
                        dict[gameMap[upper][right]] == undefined ? 1 : dict[gameMap[upper][right]] + 1;
                }
            }
            
            if (left >= 0 && gameMap[i][left] > 0) {
                count++;
                dict[gameMap[i][left]] = 
                    dict[gameMap[i][left]] == undefined ? 1 : dict[gameMap[i][left]] + 1;
            }
            if (right < width && gameMap[i][right] > 0) {
                count++;
                dict[gameMap[i][right]] = 
                    dict[gameMap[i][right]] == undefined ? 1 : dict[gameMap[i][right]] + 1;
            }
            
            if (lower < height) {
                if (left >= 0 && gameMap[lower][left] > 0) {
                    count++;
                    dict[gameMap[lower][left]] = 
                        dict[gameMap[lower][left]] == undefined ? 1 : dict[gameMap[lower][left]] + 1;
                }
                if (gameMap[lower][j] > 0) {
                    count++;
                    dict[gameMap[lower][j]] = 
                        dict[gameMap[lower][j]] == undefined ? 1 : dict[gameMap[lower][j]] + 1;
                }
                if (right < width && gameMap[lower][right] > 0) {
                    count++;
                    dict[gameMap[lower][right]] = 
                        dict[gameMap[lower][right]] == undefined ? 1 : dict[gameMap[lower][right]] + 1;
                }
            }
            
            /*
            So it is now to check which key/userId contains most value
            */
            
            let maxValue = -1;
            let maxUser = -1;
            
            for (let key in dict) {
                if (maxValue == -1 && maxUser == -1) {
                    maxUser = parseInt(key);
                    maxValue = dict[key];
                } else if (dict[key] > maxValue) {
                    maxUser = parseInt(key);
                    maxValue = dict[key];
                }
            }
            
            if (count < 2)
                tempGameMap[i][j] = 0;
            else if (count > 3)
                tempGameMap[i][j] = 0;
            else if (count == 3)
                tempGameMap[i][j] = maxUser;
            else // count == 2
                tempGameMap[i][j] = gameMap[i][j] == 0 ? 0 : maxUser;
        }
    }
    gameMap = tempGameMap;
}

function handleInput(data) {
    let userId = data.userId;
    for (var d = 0 in data.pushPosition) {
        let px = data.pushPosition[d].px;
        let py = data.pushPosition[d].py;
        
        gameMap[px][py] = userId;
    }
    io.sockets.emit('updateMap', gameMap);
}

setInterval(function(){
    recalGameMap();
    io.sockets.emit('updateMap', gameMap);
}, 1000);
    
io.on('connection', function (socket) {
    socket.emit('init', {userId: userId++, map: gameMap});
    socket.on('sendInput', function (data) {
        handleInput(data);
    });
});
