var tempData;
var socket;
var type = 0;
var user = -1;
const squareLength = 20;

$(function() {
    var myCanvas = document.getElementById("canvas");
    var mouseX = 0;
    var mouseY = 0;
    
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }
    
    var colorChose;
    var colorDict = {};
    
    var sendJson;
    
    var previewArray = [];
    
    function makeSendJson() {
        sendJson = { userId: user, pushPosition: previewArray };
    }
    
    socket = io();
    socket.on('updateMap', function (data) {
        tempData = data;
    });
    socket.on('init', function (data) {
        user = data.userId;
        tempData = data.map;
        setInterval(redrawCanvas, 1000/60);
    });
    
    myCanvas.addEventListener('mousemove', function(evt) {
        var mousePos = getMousePos(myCanvas, evt);
        mouseX = mousePos.x;
        mouseY = mousePos.y;
    });
    
    myCanvas.addEventListener('click', function(evt) {
        makeSendJson();
        socket.emit('sendInput', sendJson);
    });
    
    function redrawCanvas() {
        colorChose = $("#colorPicker").val();
        
        var c = document.getElementById("canvas");
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
         
        for(let i = 0; i <= tempData.length; i++){
            ctx.beginPath();
            ctx.moveTo(0,i * squareLength);
            ctx.lineTo(590,i * squareLength);
            ctx.lineWidth = 1;
            ctx.strokeStyle = 'Grey';
            ctx.stroke();
            if (i == tempData.length)
                break;
            
            for(let j=0; j <= tempData[i].length; j++){
                ctx.beginPath();
                ctx.moveTo(squareLength * j,0);
                ctx.lineTo(squareLength * j,380);
                ctx.lineWidth = 1;
                ctx.strokeStyle = 'Grey';
                ctx.stroke();
                
                /*
                Someone own this square
                Random color for other, synchronize color is not implemented
                Choose color for player
                */
                if (tempData[i][j] > 0) {
                    let CircleX = squareLength * j + 10;
                    let CircleY = squareLength * i + 10;
                    let radius = 7;
                
                    ctx.beginPath();
                    ctx.arc(CircleX, CircleY, radius, 0, 2 * Math.PI, false);
                    if (tempData[i][j] == user)
                        ctx.fillStyle = colorChose;
                    else if (colorDict[tempData[i][j]] != undefined)
                        ctx.fillStyle = colorDict[tempData[i][j]];
                    else {
                        colorDict[tempData[i][j]] = 'hsl(' + 360 * Math.random() + ', 50%, 50%)';
                        ctx.fillStyle = colorDict[tempData[i][j]];
                    }
                    ctx.fill();
                }
            }
        }
        
        let px = Math.floor(mouseY / squareLength);
        let py = Math.floor(mouseX / squareLength);
        
        // This px, py is array X Y, not x, y in coordinate
        
        // Get the points for selected pattern
        getPoints(px, py);
        
        // Draw the empty pattern
        for (var pt = 0 in previewArray) {
            let ptx = previewArray[pt].px;
            let pty = previewArray[pt].py;
            
            let areaXFrom = pty * squareLength;
            let areaXTo = areaXFrom + squareLength;
            let areaYFrom = ptx * squareLength;
            let areaYTo = areaYFrom + squareLength;
            
            ctx.beginPath();
            ctx.moveTo(areaXFrom,areaYFrom);
            ctx.lineTo(areaXTo,areaYFrom);
            ctx.lineTo(areaXTo,areaYTo);
            ctx.lineTo(areaXFrom,areaYTo);
            ctx.lineTo(areaXFrom,areaYFrom);
            ctx.lineWidth = 2;
            ctx.strokeStyle = 'Black';
            ctx.stroke();
        }
    }
    
    // This method return the points of selected pattern
    function getPoints(px, py) {
        var pattern = $("#selPattern").val();
        previewArray = [];
        switch(pattern) {
            case "1":
                previewArray.push({px:px, py:py});
                break;
            case "2":
                previewArray.push({px:px, py:py});
                if (py - 1 >= 0)
                    previewArray.push({px:px, py:py - 1});
                if (py + 1 < tempData[0].length)
                    previewArray.push({px:px, py:py + 1});
                break;
            case "3":
                if (px - 1 >= 0 && py - 1 >= 0)
                    previewArray.push({px:px - 1, py:py - 1});
                if (px - 1 >= 0)
                    previewArray.push({px:px - 1, py:py});
                if (px - 1 >= 0 && py + 1 < tempData[0].length)
                    previewArray.push({px:px - 1, py:py +1});
                if (py - 1 >= 0)
                    previewArray.push({px:px, py:py - 1});
                if (px + 1 < tempData.length)
                    previewArray.push({px:px + 1, py:py});
                break;
            case "4":
                previewArray.push({px:px, py:py});
                if (px - 1 >= 0 && py - 2 >= 0)
                    previewArray.push({px:px - 1, py:py - 2});
                if (px + 1 < tempData.length && py - 3 >= 0)
                    previewArray.push({px:px + 1, py:py - 3});
                if (px + 1 < tempData.length && py - 2 >= 0)
                    previewArray.push({px:px + 1, py:py - 2});
                if (px + 1 < tempData.length && py + 1 < tempData[0].length)
                    previewArray.push({px:px + 1, py:py + 1});
                if (px + 1 < tempData.length && py + 2 < tempData[0].length)
                    previewArray.push({px:px + 1, py:py + 2});
                if (px + 1 < tempData.length && py + 3 < tempData[0].length)
                    previewArray.push({px:px + 1, py:py + 3});
                break;
        }
    }
});