# Conway's Game of Life By Martin LAU

A [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) implementation using [Node.js](https://nodejs.org/en/), [Express.js](https://expressjs.com/) and [socket.io](https://socket.io/)

You can see the heroku result [here](https://nameless-refuge-84863.herokuapp.com/)

## Way to do it

1. Setup the server with express.js and socket.io
2. Setup the client with socket.io and try to connect them with JSON
3. Create a 2d array on server and implement its update logic
4. Draw the canvas with the json
5. Implement multiple user features and color
6. Implement different patterns

## Make some change to make the app support Heroku

- [Getting Started with Node on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Setup the socket.io with herokuapp.com](https://devcenter.heroku.com/articles/node-websockets#option-2-socket-io)

## Remark

- No toolbar is made but users can choose the patterns by drop down list
- Users can change their own color and color for other users is generate randomly
- px, py is the index of array but not the x, y in coordinate

## Useful link

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
